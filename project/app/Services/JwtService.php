<?php

namespace App\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use stdClass;

class JwtService
{
    const ALG = 'HS256';

    public JWT $jwt;

    public function __construct()
    {
        $this->jwt = new JWT;
    }

    public function encode(array $payload = []): string
    {
        $payload = array_merge([
            'iss' => 'lottery_game',
            'sub' => '',
            'iat' => time(),
            'exp' => time() + 60 * 60,
        ], $payload);

        return $this->jwt->encode($payload, env('JWT_KEY'), self::ALG);
    }

    public function decode(string $token): stdClass
    {
        return $this->jwt->decode($token, new Key(env('JWT_KEY'), self::ALG));
    }
}
