<?php

namespace App\Listeners;

use App\Events\AddPlayerToMatchEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PlayerIsAlreadyPlayingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ExampleEvent  $event
     * @return void
     */
    public function handle(AddPlayerToMatchEvent $event)
    {
        $playersCount = $event->match->players()
            ->where('user_id', $event->user->id)
            ->count();

        // If $playersCount > 0 - player in match
        return $playersCount;
    }
}
