<?php

namespace App\Listeners;

use App\Events\AddPlayerToMatchEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CanAddOneMorePlayerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ExampleEvent  $event
     * @return void
     */
    public function handle(AddPlayerToMatchEvent $event)
    {
        $playersCount = $event->match->players()->count();

        // if $playersCount < $game->gamer_count - you can add another player
        return $playersCount < $event->match->game->gamer_count;
    }
}
