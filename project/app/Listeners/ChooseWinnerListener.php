<?php

namespace App\Listeners;

use App\Models\LotteryGameMatch;
use App\Events\MatchFinishedEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChooseWinnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ExampleEvent  $event
     * @return void
     */
    public function handle(MatchFinishedEvent $event)
    {
        $match = LotteryGameMatch::with('game')->firstWhere('id', $event->matchId);
        $winner = $match->players()->inRandomOrder()->first();

        if ($winner) {
            DB::transaction(function () use ($match, $winner) {
                $match->update(['winner_id' => $winner->id]);
                $winner->update(['points' => $winner->points + $match->game->reward_points]);
            });
        }
    }
}
