<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use App\Services\JwtService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        if ($token === null) {
            return response()->json([
                'msg' => 'Token not found'
            ], 401);
        }

        try {
            $user = $this->verifyToken($token);
            $request->setUserResolver(fn () => User::find($user->id));
        } catch (\Exception $e) {
            return response()->json([
                'msg' => $e->getMessage()
            ], $e->getCode());
        }

        return $next($request);
    }

    protected function verifyToken(string $token)
    {
        try {
            $payload = (new JwtService)->decode($token);
        } catch (\Exception $e) {
            throw new Exception('Invalid token!', 401);
        }

        if (time() >= $payload->exp) {
            throw new Exception('Token expired!', 401);
        }

        try {
            $user = User::findOrFail($payload->sub);
        } catch (ModelNotFoundException $e) {
            throw new Exception('Invalid token!', 401);
        }

        return $user;
    }
}
