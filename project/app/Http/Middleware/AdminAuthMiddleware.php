<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class AdminAuthMiddleware extends AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');

        if ($token === null) {
            return response()->json([
                'msg' => 'Token not found'
            ], 401);
        }

        try {
            $user = $this->verifyToken($token);
            $request->setUserResolver(fn () => User::find($user->id));
        } catch (\Exception $e) {
            return response()->json([
                'msg' => $e->getMessage()
            ], $e->getCode());
        }

        if (!$user->is_admin) {
            return response()->json([
                'msg' => 'Not authorized!'
            ], 403);
        }

        return $next($request);
    }
}
