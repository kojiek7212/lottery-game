<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return response()->json([
            'users' => User::with('winnedMatches')->paginate()
        ]);
    }

    public function update(Request $request, int $id)
    {
        $user = $request->user();

        if ($id !== $user->id) {
            return response()->json([
                'msg' => 'Error!'
            ], 500);
        }

        $validated = $this->validate($request, [
            'first_name' => 'sometimes|string|min:1|max:256',
            'last_name' => 'sometimes|string|min:1|max:256',
            'password' => 'sometimes|string|min:8|max:64',
            'email' => 'sometimes|email|unique:users,email,' . $user->id . ',id',
        ]);

        $validated = array_filter($validated, function ($item) {
            return $item != null || !empty($item);
        });

        if (array_key_exists('password', $validated)) {
            $validated['password'] = Hash::make($validated['password']);
        }

        $user->update($validated);

        return response()->json($user);
    }

    public function destroy(Request $request, int $id)
    {
        $user = $request->user();

        if ($id !== $user->id) {
            return response()->json([
                'msg' => 'Error!'
            ], 500);
        }

        try {
            $user->delete();
        } catch (\Exception $e) {
            return response()->json([
                'msg' => 'Error!'
            ], 500);
        }

        return response()->json([
            'msg' => 'User was deleted!'
        ]);
    }
}
