<?php

namespace App\Http\Controllers;

use App\Events\AddPlayerToMatchEvent;
use App\Events\MatchFinishedEvent;
use App\Models\LotteryGame;
use App\Models\LotteryGameMatch;
use Illuminate\Http\Request;

class LotteryGameMatchController extends Controller
{
    public function start(Request $request)
    {
        $validated = $this->validate($request, [
            'game_id' => 'required|integer|exists:lottery_games,id',
            'start_datetime' => 'required|date|date_format:Y-m-d H:i:s|after_or_equal:now',
        ]);

        $validated = array_merge($validated, $this->parseDatetime($validated['start_datetime']));

        $lotteryGame = LotteryGame::findOrFail($validated['game_id']);

        try {
            $match = $lotteryGame->lotteryGameMatches()->create($validated);
        } catch (\Exception $e) {
            return response()->json([
                'msg' => 'Error when creating a match!',
                'debug' => $e->getMessage()
            ], 500);
        }

        return response()->json($match);
    }

    protected function parseDatetime(string $datetime)
    {
        return [
            'start_date' => explode(' ', $datetime)[0],
            'start_time' => explode(' ', $datetime)[1],
        ];
    }

    public function stop(Request $request)
    {
        $validated = $this->validate($request, [
            'match_id' => 'required|integer|exists:lottery_game_matches,id'
        ]);

        $match = LotteryGameMatch::find($validated['match_id']);

        if ($match->is_finished) {
            return response()->json([
                'msg' => 'The match has already ended'
            ]);
        }

        $match->update(['is_finished' => true]);

        event(new MatchFinishedEvent($match->id));

        return response()->json([
            'msg' => 'The match was stopped'
        ]);
    }

    public function addPlayer(Request $request)
    {
        $validated = $this->validate($request, [
            'match_id' => 'required|integer|exists:lottery_game_matches,id'
        ]);

        $match = LotteryGameMatch::find($validated['match_id']);

        if ($match->is_finished) {
            return response()->json([
                'msg' => 'The match has already been completed!'
            ]);
        }

        $user = $request->user();

        $results = event(new AddPlayerToMatchEvent($user, $match));

        // Result CheckPlayerInMatchListener class
        if ($results[0]) {
            return response()->json([
                'msg' => 'You are already a participant in this match!'
            ]);
        }

        // Result CanAddOneMorePlayerListener class
        if (!$results[1]) {
            return response()->json([
                'msg' => 'The required number of players has already been recruited!'
            ]);
        }

        try {
            $user->lotteryGameMatches()->attach($validated['match_id']);
        } catch (\Exception $e) {
            return response()->json([
                'msg' => 'Error adding to the match!'
            ]);
        }

        return response()->json([
            'msg' => 'You have been added to the match!'
        ]);
    }

    public function allByGameId(Request $request)
    {
        $validated = $this->validate($request, [
            'lottery_game_id' => 'required|integer|exists:lottery_games,id'
        ]);

        $lotteryGame = LotteryGameMatch::with(['game', 'winner'])
            ->where('game_id', $validated['lottery_game_id'])->paginate();

        return response()->json($lotteryGame);
    }
}
