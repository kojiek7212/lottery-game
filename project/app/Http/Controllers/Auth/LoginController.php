<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Services\JwtService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validated = $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|string',
        ]);

        $user = User::firstWhere('email', $validated['email']);

        if ($user === null) {
            return response()->json([
                'msg' => 'User not found!'
            ], 404);
        }

        if (!$this->checkPassword($validated['password'], $user->password)) {
            return response()->json([
                'msg' => 'Email or password are wrong!'
            ], 500);
        }

        return response()->json(
            [
                'user' => $user
            ],
            200,
            [
                'Authorization' => (new JwtService)->encode([
                    'sub' => $user->id
                ]),
            ]
        );
    }

    public function logout(Request $request)
    {
        return response()->json(
            ['msg' => 'Logout'],
            200,
            [
                'Authorization' => (new JwtService)->encode([
                    'sub' => $request->user()->id,
                    'exp' => 1
                ]),
            ]
        );
    }

    protected function checkPassword(string $password, string $hash)
    {
        return Hash::check($password, $hash);
    }
}
