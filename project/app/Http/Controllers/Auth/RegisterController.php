<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Services\JwtService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $validated = $this->validate($request, [
            'first_name' => 'required|string|min:1|max:256',
            'last_name' => 'required|string|min:1|max:256',
            'password' => 'required|string|min:8|max:64',
            'email' => 'required|email|unique:users,email',
        ]);

        $validated['password'] = Hash::make($validated['password']);

        $user = User::create($validated);

        return response()->json(
            [
                'user' => $user
            ],
            200,
            [
                'Authorization' => (new JwtService)->encode([
                    'sub' => $user->id
                ]),
            ]
        );
    }
}
