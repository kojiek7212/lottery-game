<?php

namespace App\Http\Controllers;

use App\Models\LotteryGame;

class LotteryGameController extends Controller
{
    public function index()
    {
        return response()->json([
            'lottery_games' => LotteryGame::with([
                'lotteryGameMatches' => function ($q) {
                    $q->orderBy('start_date', 'desc')
                        ->orderBy('start_time', 'asc');
                }
            ])->paginate()
        ]);
    }
}
