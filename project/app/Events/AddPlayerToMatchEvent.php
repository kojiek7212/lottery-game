<?php

namespace App\Events;

use App\Models\LotteryGameMatch;
use App\Models\User;

class AddPlayerToMatchEvent extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        public User $user,
        public LotteryGameMatch $match
    ) {
        //
    }
}
