<?php

namespace App\Events;

class MatchFinishedEvent extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        public int $matchId
    ) {
        //
    }
}
