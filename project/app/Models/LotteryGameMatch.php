<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LotteryGameMatch extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'game_id', 'start_date', 'start_time', 'winner_id', 'is_finished'
    ];

    public function winner()
    {
        return $this->belongsTo(User::class, 'winner_id', 'id');
    }

    public function players()
    {
        return $this->belongsToMany(
            User::class,
            'lottery_game_match_users',
            'lottery_game_match_id',
            'user_id'
        );
    }

    public function game()
    {
        return $this->belongsTo(LotteryGame::class, 'game_id', 'id');
    }

    public function scopeFinished($query)
    {
        return $query->where('is_finished', true);
    }

    public function scopeNotFinished($query)
    {
        return $query->where('is_finished', false);
    }
}
