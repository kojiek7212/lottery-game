<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\MatchFinishedEvent::class => [
            \App\Listeners\ChooseWinnerListener::class,
        ],
        \App\Events\AddPlayerToMatchEvent::class => [
            \App\Listeners\PlayerIsAlreadyPlayingListener::class,
            \App\Listeners\CanAddOneMorePlayerListener::class,
        ]
    ];

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
