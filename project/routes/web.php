<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Http\Controllers\Auth\RegisterController;
use App\Models\LotteryGame;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->post('api/users/register', 'Auth\RegisterController@register');
$router->post('api/users/login', 'Auth\LoginController@login');

$router->get('/api/lottery_games', 'LotteryGameController@index');
$router->get('/api/lottery_game_matchs', 'LotteryGameMatchController@allByGameId');

$router->group([
    'middleware' => 'auth'
], function () use ($router) {
    $router->post('/api/lottery_game_match_users', 'LotteryGameMatchController@addPlayer');
    $router->put('/api/users/{id}', 'UserController@update');
    $router->delete('/api/users/{id}', 'UserController@destroy');
    $router->post('/api/users/logout', 'Auth\LoginController@logout');
});

$router->group([
    'middleware' => 'auth_admin'
], function () use ($router) {
    $router->put('/api/lottery_game_matchs', 'LotteryGameMatchController@stop');
    $router->post('/api/lottery_game_matchs', 'LotteryGameMatchController@start');
    $router->get('/api/users', 'UserController@index');
});
