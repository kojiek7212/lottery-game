<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lottery_game_matches', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('game_id');
            $table->date('start_date')->default(Carbon::now()->format('Y-m-d'));
            $table->time('start_time')->default(Carbon::now()->format('H:i:s'));
            $table->unsignedBigInteger('winner_id')->nullable();
            $table->boolean('is_finished')->default(false);

            $table->foreign('game_id')->references('id')->on('lottery_games')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('winner_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lottery_game_matches');
    }
};
