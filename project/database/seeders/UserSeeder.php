<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->createAdmin();
        User::factory(10)->create();
    }

    protected function createAdmin()
    {
        User::create([
            'first_name' => 'Admin',
            'last_name' => 'Ivanov',
            'password' => Hash::make('password'),
            'email' => 'admin@admin.com',
            'is_admin' => true,
            'points' => 0
        ]);
    }
}
