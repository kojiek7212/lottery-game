# Lottery game

В папке ```deploy``` выполнить команды
```
cp .\.env.example .\.env
docker-compose -p <ИМЯ ПРОЕКТА> up -d
```

Подключиться к контейнеру ```lottery_game_app```
```
docker exec -it <ID контейнера> bash
```

Установить пакеты
```
composer install
```

Создать файл ```.env```
```
cp .env.example .env
```
В файле ```.env``` добавить рандомную строку в переменную ```JWT_KEY```

Выполнить миграцию и заполнить базу данными
```
php artisan migrate --seed
```
